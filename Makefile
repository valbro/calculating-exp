MPICC = g++
CC = icpc
CCFLAGS = -O3 -mavx -I $(INCLUDE) -Wall -g3

PROGRAM_DIR = ./
SOURCE = main.o

INCLUDE =  $(PROGRAM_DIR)

all : $(SOURCE)
	$(MPICC) -o out $(SOURCE) $(CCFLAGS)


clean:
	rm *.o

main.o : main.cpp
	$(MPICC) -c main.cpp $(CCFLAGS)
