#ifndef _AVX_H_
#define _AVX_H_

#include "immintrin.h"

namespace avx {

	const size_t EXP_POLY_DEGREE = 5;
	const size_t LOG_POLY_DEGREE = 5;

	union f4 {
		float f[4];
		unsigned int u[4];
		__m256 m;
		__m256i mi;
	};
	typedef union f4 f4_t;

	union d2 {
		double d[2];
		unsigned long long l[2];
		__m256 m;
		__m256i mi;
		__m256d md;
	};
	typedef union d2 d2_t;

	inline __m256 exp(__m256 x);
	inline __m256 log(__m256 x);
	inline __m256 pow(__m256 x, __m256 y);

	inline __m256d exp(__m256d x);
	inline __m256d log(__m256d x);
	inline __m256d pow(__m256d x, __m256d y);

	inline d2_t exp(d2_t x);
	inline d2_t log(d2_t x);
	inline d2_t pow(d2_t x, d2_t y);

}

#include "avx_impl.hpp"

#endif
