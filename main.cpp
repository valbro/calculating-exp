#include <iostream>
#include "avx.hpp"
#include "immintrin.h"

int main() {
	for(size_t i = 0; i < 100; ++i) {
		float p[8];
		for(size_t k = 0;  k < 8; ++k) {
			p[k] = -0.5 + i * 0.01;
		}
		__m256 x = _mm256_load_ps(p);
		__m256 e = avx::polyExp<5>(x);
		_mm256_store_ps(p, e);
		_mm256_zeroupper();
		std::cout << *p << " " << p[1] << " " << p[2] << " " << p[3] << std::endl;
	}	
}
